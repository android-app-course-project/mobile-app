package com.tuneit.minions.chatapp.utils

import org.json.JSONObject
import org.junit.Assert
import org.junit.Test

internal class JwtUtilsKtTest {

    @Test
    fun decodeToken() {
        val jwt = "eyJhbGciOiJSUzUxMiJ9.eyJ1c2VybmFtZSI6InRlc3QyIiwiZXhwIjoxNjg1MTkwMTk4fQ.WNMzBgidrv6jYCmPLBOBoXhnQfDSeGUbpHA4389KubJlbyMK0STTMXzzDB9iDDvm6fZ3DGMV-E8RvgT8ZMPFS0_X8VnWpn6ox4xC8Rkk7WZqqN9uGwUku08EUUDvNaJH754gqrhzG-UpFyjT7cPHM6I7r63DTMOF1s8HqYa74xVdZWdnXuGOisBYQxfDbspgRbYa3TwwYJhJaWcXC7PeVgRKaDLA4Y7l4VSmk0nqYSX50L65mIzBcuLM966zaoE7t6H8b3aKZIT_64uAqyabdO1oPfyBZvHRa20gbmkCgT1D0zMSHV2z6XMwOV1-l3gfLqTTYTEz2f_Lk8qpWfo1kQ";
        val o: JSONObject = com.tuneit.minions.chatapp.utils.decodeTokenPayload(jwt)

        Assert.assertEquals(o.getString("username"), "test2");
        Assert.assertEquals(o.getLong("exp"), 1685190198)
    }
}
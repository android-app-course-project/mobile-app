package com.tuneit.minions.chatapp.data.model.chat

import kotlinx.serialization.Serializable

@Serializable
data class ChatDescription (
    val id: Long,
    val user1: String,
    val user2: String,
    val status: String,
    val start: String,
)
package com.tuneit.minions.chatapp.data.model.rate

import kotlinx.serialization.Serializable

@Serializable
data class RateRequest(
    val chatId: Long,
    val isPositive: Boolean,
)
package com.tuneit.minions.chatapp

import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.runtime.Composable
import androidx.datastore.preferences.preferencesDataStore
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.tuneit.minions.chatapp.data.repository.UserRepository
import com.tuneit.minions.chatapp.ui.screens.chat.ChatScreen
import com.tuneit.minions.chatapp.ui.screens.login.LoginScreen
import com.tuneit.minions.chatapp.ui.screens.mainMenu.MainMenuScreen
import com.tuneit.minions.chatapp.ui.screens.signUp.SignUpScreen
import com.tuneit.minions.chatapp.ui.theme.ChatappTheme
import dagger.hilt.android.AndroidEntryPoint

//private const val USER_PREFERENCES_NAME = "user_preferences"
//
//private val Context.dataStore by preferencesDataStore(
//    name = USER_PREFERENCES_NAME,
//)

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ChatappTheme {
                AppNavigator()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @Composable
    private fun AppNavigator() {
        val navController = rememberNavController()

        NavHost(navController = navController, startDestination = "login") {
            composable("login") {
                LoginScreen(navController = navController)
            }
            composable("signUp") {
                SignUpScreen(navController = navController)
            }
            composable("main") {
                MainMenuScreen(navController = navController)
            }
            composable("chat") {
                ChatScreen(navController = navController)
            }
        }
    }
}


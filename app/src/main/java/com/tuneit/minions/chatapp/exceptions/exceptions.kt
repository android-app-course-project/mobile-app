package com.tuneit.minions.chatapp.exceptions

class InvalidTokenException(message: String): Exception(message)
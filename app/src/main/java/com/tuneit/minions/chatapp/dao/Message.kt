package com.tuneit.minions.chatapp.dao

import com.tuneit.minions.chatapp.data.model.chat.ChatMessage

data class Message(
    val text: String,
    val fromMe: Boolean,
)
package com.tuneit.minions.chatapp.ui.screens.chat

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tuneit.minions.chatapp.dao.Message
import com.tuneit.minions.chatapp.data.model.chat.ChatDescription
import com.tuneit.minions.chatapp.data.model.chat.ChatMessage
import com.tuneit.minions.chatapp.data.model.chat.ChatMessageRequest
import com.tuneit.minions.chatapp.data.model.chat.ChatMessagesResponse
import com.tuneit.minions.chatapp.data.model.rate.RateRequest
import com.tuneit.minions.chatapp.data.network.ChatApi
import com.tuneit.minions.chatapp.data.network.ChatMessageAPI
import com.tuneit.minions.chatapp.data.repository.UserRepository
import com.tuneit.minions.chatapp.exceptions.InvalidTokenException
import com.tuneit.minions.chatapp.utils.decodeTokenPayload
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.json.JSONException
import retrofit2.HttpException
import javax.inject.Inject

data class ChatState(
    val token: String = "",
    val messages: List<Message> = listOf(),
    val me: String = "",
    val interlocutor: String = "",
    val chatId: Long? = null,
    val isChatEnabled: Boolean = true,
    val remainingTime: Long = 60,
    val isRateDialogExpanded: Boolean = false,
    val isChatFinished: Boolean = false,
)

@HiltViewModel
class ChatViewModel @Inject constructor(
    private val userRepository: UserRepository
): ViewModel() {
    private val _uiState = MutableStateFlow(ChatState())
    val uiState: StateFlow<ChatState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            try {
                userRepository.dataFlow.collect { data ->
                    _uiState.update { state -> state.copy(
                        token = data.accessToken,
                        me = data.me,
                        interlocutor = data.interlocutor,
                        chatId = data.chatId.toLong(),
                    ) }
                }
            } catch (e: Exception) {
                Log.e(TAG, e.message ?: "")
            }
        }
    }

    fun sendMessage(msg: String) {
        try {
            viewModelScope.launch {
                ChatMessageAPI.retrofitService.sendMessage(
                    uiState.value.token,
                    uiState.value.me,
                    ChatMessageRequest(
                        content = msg,
                        sendTime = "2023-05-25T20:20",
                        offset = 0,
                        chat = uiState.value.chatId!!.toLong(),
                    ),
                )
            }
        } catch (e: HttpException) {
            Log.e(TAG, "sendMessage failed: ${e.code()}")
        }
    }

    fun getMessages() {
        viewModelScope.launch {
            try {
                val chatMessages: ChatMessagesResponse =
                    ChatMessageAPI.retrofitService.getMessages(
                        0,
                        uiState.value.token,
                        uiState.value.me,
                    )
                val newMessages = ArrayList<Message>()
                chatMessages.messages.forEach { chatMessage ->
                    newMessages.add(
                        Message(
                            text = chatMessage.content,
                            fromMe = chatMessage.sender == uiState.value.me,
                        )
                    )
                }
                _uiState.update { state -> state.copy(messages = newMessages) }
            } catch (e: HttpException) {
                Log.e(TAG, "getMessages failed: ${e.code()}")
            }
        }
    }

    fun disableChat() {
        viewModelScope.launch {
            try {
                val chatDescription = ChatApi.retrofitService.getChatByUsername(
                    uiState.value.token,
                    uiState.value.me,
                )
                ChatApi.retrofitService.updateChat(
                    uiState.value.token,
                    ChatDescription(
                        chatDescription.id,
                        chatDescription.user1,
                        chatDescription.user2,
                        "ARCHIVED",
                        chatDescription.start,
                    )
                )
                _uiState.update { state -> state.copy(isChatEnabled = false) }
            } catch (e: HttpException) {
                Log.e(TAG, e.message())
            }
        }
    }

    fun decrementRemainingTime() {
        val currentRemainingTime = uiState.value.remainingTime - 1;
        _uiState.update { state -> state.copy(remainingTime = currentRemainingTime) }
    }

    fun expandRateDialog() {
        _uiState.update { state ->
            state.copy(isRateDialogExpanded = true)
        }
    }

    fun gradeUser(isPositive: Boolean) {
        viewModelScope.launch {
            try {
                ChatApi.retrofitService.gradeUser(
                    uiState.value.token,
                    uiState.value.me,
                    RateRequest(
                        uiState.value.chatId!!,
                        isPositive,
                    )
                )
            } catch (e: HttpException) {
                Log.e(TAG, e.message())
            } finally {
                _uiState.update { state -> state.copy(
                    isRateDialogExpanded = false,
                    isChatFinished = true
                ) }
            }
        }
    }
}
package com.tuneit.minions.chatapp.data.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.tuneit.minions.chatapp.data.model.auth.AuthRequest
import com.tuneit.minions.chatapp.data.model.auth.AuthResponse
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.Body

import retrofit2.http.Headers
import retrofit2.http.POST

private const val BASE_URL = "http://158.160.109.166:8082/api/v1/auth/"


@OptIn(ExperimentalSerializationApi::class)
private val retrofit = Retrofit.Builder()
    .addConverterFactory(Json.asConverterFactory(MediaType.get("application/json")))
    .baseUrl(BASE_URL)
    .build()


interface AuthService {
    @Headers("Content-Type: application/json")
    @POST("login")
    suspend fun login(@Body authRequest: AuthRequest): Response<Unit>

    @Headers("Content-Type: application/json")
    @POST("register")
    suspend fun register(@Body authRequest: AuthRequest): Response<Unit>
}


object AuthApi {
    val retrofitService: AuthService by lazy {
        retrofit.create(AuthService::class.java)
    }
}
package com.tuneit.minions.chatapp.ui.screens.login


import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tuneit.minions.chatapp.data.repository.UserRepository
import com.tuneit.minions.chatapp.data.model.auth.AuthRequest
import com.tuneit.minions.chatapp.data.network.AuthApi
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

data class LoginState (
    var username: String = "",
    var password: String = "",
    var isLoading: Boolean = false,
    var isAuthenticated: Boolean = false,
    var error: String? = null,
)

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val userRepository: UserRepository
): ViewModel() {

    private val _uiState = MutableStateFlow(LoginState())
    val uiState: StateFlow<LoginState> = _uiState.asStateFlow()

    fun updateUsername(username: String) {
        _uiState.update { state -> state.copy(username = username) }
    }

    fun updatePassword(password: String) {
        _uiState.update { state -> state.copy(password = password) }
    }

    fun login() {
        viewModelScope.launch {
            try {
                _uiState.update { state -> state.copy(isLoading = true) }
                val response = AuthApi.retrofitService.login(
                    AuthRequest(
                        username = uiState.value.username,
                        password = uiState.value.password
                    )
                )
                if (response.code() != 200) throw HttpException(response);
                val authHeader = response.headers().get("Set-Cookie")
                   ?: throw Exception("No Authorization header in response")

                val jwt = authHeader.substring(14)
                userRepository.updateAccessToken(jwt)
                _uiState.update { state -> state.copy(isAuthenticated = true) }
            } catch (e: HttpException) {
                when (e.code()) {
                    401 -> _uiState.update { state -> state.copy(error = "Incorrect password or login") }
                    else -> _uiState.update { state -> state.copy(error = e.message()) }
                }
            } catch (e: Exception) {
                _uiState.update { state -> state.copy(error = e.message) }
            } finally {
                _uiState.update { state -> state.copy(isLoading = false) }
            }
        }
    }
}

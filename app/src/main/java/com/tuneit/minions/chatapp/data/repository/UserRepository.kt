package com.tuneit.minions.chatapp.data.repository

import android.content.ContentValues.TAG
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject

data class UserData(
    val accessToken: String,
    val me: String,
    val interlocutor: String,
    val chatId: String,
)

class UserRepository @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {

    private object PreferencesKeys {
        val ACCESS_TOKEN = stringPreferencesKey("access_token")
        val ME = stringPreferencesKey("me")
        val INTERLOCUTOR = stringPreferencesKey("interlocutor")
        val CHAT_ID = stringPreferencesKey("chat_id")
    }

    val dataFlow: Flow<UserData> = dataStore.data
        .catch { exception ->
            if (exception is IOException) {
                Log.e(TAG, "Error reading preferences.", exception)
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map { preferences ->
            mapUserPreferences(preferences)
        }

    suspend fun updateAccessToken(accessToken: String) {
        dataStore.edit { preferences ->
            preferences[PreferencesKeys.ACCESS_TOKEN] = accessToken
        }
    }

    suspend fun updateChatPreferences(me: String, interlocutor: String, chatId: String) {
        dataStore.edit { preferences ->
            preferences[PreferencesKeys.INTERLOCUTOR] = interlocutor
            preferences[PreferencesKeys.CHAT_ID] = chatId
            preferences[PreferencesKeys.ME] = me
        }
    }

    private fun mapUserPreferences(preferences: Preferences): UserData {
        val accessToken = preferences[PreferencesKeys.ACCESS_TOKEN] ?: ""
        val me = preferences[PreferencesKeys.ME] ?: ""
        val interlocutor = preferences[PreferencesKeys.INTERLOCUTOR] ?: ""
        val chatId = preferences[PreferencesKeys.CHAT_ID] ?: ""
        return UserData(accessToken, me, interlocutor, chatId)
    }
}
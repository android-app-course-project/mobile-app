package com.tuneit.minions.chatapp.ui.screens.signUp

import android.content.ContentValues
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tuneit.minions.chatapp.data.model.auth.AuthRequest
import com.tuneit.minions.chatapp.data.network.AuthApi
import com.tuneit.minions.chatapp.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

data class SignUpState (
    var username: String = "",
    var password: String = "",
    var isLoading: Boolean = false,
    var isSignedUp: Boolean = false,
    var error: String? = null,
)

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val userRepository: UserRepository
): ViewModel() {
    private val _uiState = MutableStateFlow(SignUpState())
    val uiState: StateFlow<SignUpState> = _uiState.asStateFlow()

    fun updateUsername(username: String) {
        _uiState.update { state -> state.copy(username = username) }
    }

    fun updatePassword(password: String) {
        _uiState.update { state -> state.copy(password = password) }
    }

    fun signUp() {
        viewModelScope.launch {
            try {
                _uiState.update { state -> state.copy(isLoading = true) }
                AuthApi.retrofitService.register(
                    AuthRequest(
                        username = uiState.value.username,
                        password = uiState.value.password
                    )
                )
                _uiState.update { state -> state.copy(isSignedUp = true) }
            } catch (e: HttpException) {
                Log.e(ContentValues.TAG, e.message, e)
                when (e.code()) {
                    400 -> _uiState.update { state -> state.copy(error = "Invalid values") }
                    403 -> _uiState.update { state -> state.copy(error = "User already exists") }
                    else -> _uiState.update { state -> state.copy(error = e.message()) }
                }
            } catch (e: Exception) {
                Log.e(ContentValues.TAG, e.message, e)
                _uiState.update { state -> state.copy(error = e.message) }
            } finally {
                _uiState.update { state -> state.copy(isLoading = false) }
            }
        }
    }
}
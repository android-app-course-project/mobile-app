package com.tuneit.minions.chatapp.data.model.chat

import kotlinx.serialization.Serializable

@Serializable
data class ChatMessageRequest (
    val content: String,
    val sendTime: String,
    val offset: Long,
    val chat: Long,
)
package com.tuneit.minions.chatapp.ui.screens.chat

import androidx.compose.foundation.background
import androidx.compose.foundation.focusable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.tuneit.minions.chatapp.dao.Message
import com.tuneit.minions.chatapp.ui.components.ChatHeader
import com.tuneit.minions.chatapp.ui.components.ChatInput
import com.tuneit.minions.chatapp.ui.components.RateDialog
import com.tuneit.minions.chatapp.ui.components.ReceivedMessageRow
import com.tuneit.minions.chatapp.ui.components.SentMessageRow
import kotlinx.coroutines.delay


@Composable
fun ChatScreen(
    navController: NavController,
    viewModel: ChatViewModel = hiltViewModel(),
) {
    val uiState by viewModel.uiState.collectAsState()

    val scrollState = rememberLazyListState(initialFirstVisibleItemIndex = uiState.messages.size)

    LaunchedEffect(uiState.messages) {
        if (uiState.messages.isNotEmpty()) {
            scrollState.scrollToItem(uiState.messages.size - 1)
        }
    }

    LaunchedEffect(uiState.isChatEnabled) {
        if (uiState.isChatEnabled) {
            repeat(100) {
                viewModel.getMessages()
                delay(500)
            }
        }
    }

    LaunchedEffect(uiState.isChatEnabled) {
        if (uiState.isChatEnabled) {
            while (uiState.remainingTime > 0) {
                viewModel.decrementRemainingTime()
                delay(1000)
            }
            viewModel.disableChat()
            viewModel.expandRateDialog()
        }
    }

    LaunchedEffect(uiState.isChatFinished) {
        if (uiState.isChatFinished) {
            navController.navigate("main")
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .focusable()
            .wrapContentHeight()
            .imePadding()
            .background(MaterialTheme.colorScheme.background),
    ) {
        if (uiState.isRateDialogExpanded) {
            RateDialog(
                onDislike = {
                    viewModel.gradeUser(false)
                },
                onLike = {
                    viewModel.gradeUser(true)
                }
            )
        }

        ChatHeader(
            interlocutor = uiState.interlocutor,
            timer = uiState.remainingTime.toString(),
        )
        LazyColumn(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Bottom,
            state = scrollState
        ) {
            items(uiState.messages) { message: Message ->
                when (message.fromMe) {
                    true -> {
                        SentMessageRow(
                            text = message.text,
                        )
                    }
                    false -> {
                        ReceivedMessageRow(
                            text = message.text,
                        )
                    }
                }
            }

        }
        ChatInput(
            onMessageChange = {message -> viewModel.sendMessage(message)}
        )
    }
}

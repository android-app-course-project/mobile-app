package com.tuneit.minions.chatapp.data.model.chat

import kotlinx.serialization.Serializable

@Serializable
data class ChatMessage (
    val id: Long,
    val sender: String,
    val content: String,
    val sendTime: String,
    val offset: Long,
    val chatId: Long,
)
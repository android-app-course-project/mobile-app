package com.tuneit.minions.chatapp.data.model.rate

import kotlinx.serialization.Serializable

@Serializable
data class GetRateResponse (
    val username: String,
    val rating: Long,
)
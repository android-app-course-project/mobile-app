package com.tuneit.minions.chatapp.utils

import android.os.Build
import androidx.annotation.RequiresApi
import com.tuneit.minions.chatapp.exceptions.InvalidTokenException
import org.json.JSONObject
import java.util.Base64

@RequiresApi(Build.VERSION_CODES.O)
fun decodeTokenPayload(jwt: String): JSONObject {
    val parts = jwt.split('.')
    if (parts.size != 3) throw InvalidTokenException("Incorrect JWT structure")
    return JSONObject(
        String(Base64.getDecoder().decode(parts[1]))
    )
}
package com.tuneit.minions.chatapp.data.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.tuneit.minions.chatapp.data.model.chat.ChatDescription
import com.tuneit.minions.chatapp.data.model.rate.GetRateResponse
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Query


private const val BASE_URL = "http://158.160.109.166:8082/api/v1/chat-service/"

@OptIn(ExperimentalSerializationApi::class)
private val retrofit = Retrofit.Builder()
    .addConverterFactory(Json.asConverterFactory(MediaType.get("application/json")))
    .baseUrl(BASE_URL)
    .build()


interface ChatInquiryService {
    @POST("chat-request")
    suspend fun createChatRequest(
        @Header("Authorization") authorization: String,
        @Header("username") username: String,
    )

    @DELETE("chat-request")
    suspend fun cancelChatRequest(
        @Header("Authorization") authorization: String,
        @Header("username") username: String,
    )
}


object ChatInquiryAPI {
    val retrofitService: ChatInquiryService by lazy {
        retrofit.create(ChatInquiryService::class.java)
    }
}
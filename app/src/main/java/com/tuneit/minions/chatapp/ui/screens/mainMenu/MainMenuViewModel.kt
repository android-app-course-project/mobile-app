package com.tuneit.minions.chatapp.ui.screens.mainMenu

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tuneit.minions.chatapp.data.model.chat.ChatDescription
import com.tuneit.minions.chatapp.data.network.ChatService
import com.tuneit.minions.chatapp.data.network.ChatApi
import com.tuneit.minions.chatapp.data.network.ChatInquiryAPI
import com.tuneit.minions.chatapp.data.repository.UserRepository
import com.tuneit.minions.chatapp.exceptions.InvalidTokenException
import com.tuneit.minions.chatapp.utils.decodeTokenPayload
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.json.JSONException
import retrofit2.HttpException
import javax.inject.Inject

data class MainMenuState (
    var token: String = "",
    var username: String = "",
    var rating: Long = 0,
    var isAuthenticated: Boolean = true,
    var isSearchGoingOn: Boolean = false,
    var isInterlocutorFound: Boolean = false,
)

@RequiresApi(Build.VERSION_CODES.O)
@HiltViewModel
class MainMenuViewModel @Inject constructor(
    private val userRepository: UserRepository
): ViewModel() {

    private val _uiState = MutableStateFlow(MainMenuState())
    val uiState: StateFlow<MainMenuState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            try {
                userRepository.dataFlow.collect { data ->
                    val username = decodeTokenPayload(data.accessToken).getString("username")
                    val rateResponse = ChatApi.retrofitService.getRating(data.accessToken, username)

                    _uiState.update { state ->
                        state.copy(
                            token = data.accessToken,
                            username = username,
                            rating = rateResponse.rating,
                        )
                    }
                }
            } catch (e: Exception) {
                when(e) {
                    is InvalidTokenException,
                    is JSONException -> {
                        Log.e(TAG, e.message, e)
                        _uiState.update { state -> state.copy(
                            isAuthenticated = false
                        ) }
                        userRepository.updateAccessToken("")
                    }
                    else -> throw e
                }
            }
        }
    }

    fun logout() {
        viewModelScope.launch {
            _uiState.update { state -> state.copy(
                isAuthenticated = false
            ) }
            userRepository.updateAccessToken("")
        }
    }

    fun startSearching() {
        viewModelScope.launch {
            try {
                _uiState.update { state -> state.copy(isSearchGoingOn = true) }
                ChatInquiryAPI.retrofitService.createChatRequest(uiState.value.token, uiState.value.username)
                Log.e(TAG, "createChatRequest initiated")
            } catch (e: HttpException) {
                _uiState.update { state -> state.copy(isSearchGoingOn = false) }
                Log.e(TAG, "createChatRequest failed: ${e.message()}")
            }
        }
    }

    fun stopSearching() {
        viewModelScope.launch {
            try {
                ChatInquiryAPI.retrofitService.cancelChatRequest(uiState.value.token, uiState.value.username)
                Log.e(TAG, "createChatRequest canceled")
            } catch (e: HttpException) {
                Log.e(TAG, "Cancel chat creating failed: ${e.message()}")
            } finally {
                _uiState.update { state -> state.copy(isSearchGoingOn = false) }
            }
        }
    }

    fun getChat() {
        viewModelScope.launch {
            try {
                val chat: ChatDescription =
                    ChatApi.retrofitService.getChatByUsername(uiState.value.token, uiState.value.username)
                Log.e(TAG, "Chat created: [${chat.id}, ${chat.user1}, ${chat.user2}]")

                val interlocutor = if (uiState.value.username == chat.user1) chat.user2
                else chat.user1

                userRepository.updateChatPreferences(uiState.value.username, interlocutor, chat.id.toString())
                _uiState.update { state -> state.copy(isSearchGoingOn = false, isInterlocutorFound = true) }
            } catch (e: HttpException) {
                Log.e(TAG, "Get chat request failed: ${e.message()}")
            }
        }
    }

    fun updateRating() {
        viewModelScope.launch {
            try {
                val rateResponse = ChatApi.retrofitService.getRating(
                    uiState.value.token,
                    uiState.value.username,
                )
                _uiState.update { state -> state.copy(rating = rateResponse.rating) }
            } catch (e: HttpException) {
                Log.e(TAG, e.message())
            }
        }
    }
}

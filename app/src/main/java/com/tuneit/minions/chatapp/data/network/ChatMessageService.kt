package com.tuneit.minions.chatapp.data.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.tuneit.minions.chatapp.data.model.chat.ChatMessage
import com.tuneit.minions.chatapp.data.model.chat.ChatMessageRequest
import com.tuneit.minions.chatapp.data.model.chat.ChatMessagesResponse
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query


private const val BASE_URL = "http://158.160.109.166:8082/api/v1/chat-service/"

@OptIn(ExperimentalSerializationApi::class)
private val retrofit = Retrofit.Builder()
    .addConverterFactory(Json.asConverterFactory(MediaType.get("application/json")))
    .baseUrl(BASE_URL)
    .build()


interface ChatMessageService {

    @Headers("Content-Type: application/json")
    @POST("chat_message")
    suspend fun sendMessage(
        @Header("Authorization") authorization: String,
        @Header("username") username: String,
        @Body chatMessageRequest: ChatMessageRequest,
    ): ChatMessage

    @GET("chat_message/messages")
    suspend fun getMessages(
        @Query("offset") offset: Long,
        @Header("Authorization") authorization: String,
        @Header("username") username: String,
    ): ChatMessagesResponse
}


object ChatMessageAPI {
    val retrofitService: ChatMessageService by lazy {
        retrofit.create(ChatMessageService::class.java)
    }
}
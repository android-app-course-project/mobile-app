package com.tuneit.minions.chatapp.data.model.auth

import kotlinx.serialization.Serializable

@Serializable
data class AuthResponse (
    val jwt: String,
)
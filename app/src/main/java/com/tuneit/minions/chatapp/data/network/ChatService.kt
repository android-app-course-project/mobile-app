package com.tuneit.minions.chatapp.data.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.tuneit.minions.chatapp.data.model.chat.ChatDescription
import com.tuneit.minions.chatapp.data.model.rate.GetRateResponse
import com.tuneit.minions.chatapp.data.model.rate.RateRequest
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header

import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Query

private const val BASE_URL = "http://158.160.109.166:8082/api/v1/chat-service/"


@OptIn(ExperimentalSerializationApi::class)
private val retrofit = Retrofit.Builder()
    .addConverterFactory(Json.asConverterFactory(MediaType.get("application/json")))
    .baseUrl(BASE_URL)
    .build()


interface ChatService {
    @Headers("Content-Type: application/json")
    @GET("chat/rating")
    suspend fun getRating(
        @Header("Authorization") authorization: String,
        @Header("username") username: String,
    ): GetRateResponse

    @GET("chat")
    suspend fun getChatByUsername(
        @Header("Authorization") authorization: String,
        @Header("username") username: String,
    ) : ChatDescription

    @PUT("chat")
    suspend fun updateChat(
        @Header("Authorization") authorization: String,
        @Body chatDescription: ChatDescription,
    ) : ChatDescription

    @POST("chat/grade")
    suspend fun gradeUser(
        @Header("Authorization") authorization: String,
        @Header("username") username: String,
        @Body rateRequest: RateRequest,
    )
}


object ChatApi {
    val retrofitService: ChatService by lazy {
        retrofit.create(ChatService::class.java)
    }
}
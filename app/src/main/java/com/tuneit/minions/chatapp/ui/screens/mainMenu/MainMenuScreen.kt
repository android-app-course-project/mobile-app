package com.tuneit.minions.chatapp.ui.screens.mainMenu

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ExitToApp
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import kotlinx.coroutines.delay

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun MainMenuScreen(
    navController: NavController,
    viewModel: MainMenuViewModel = hiltViewModel(),
) {
    val uiState by viewModel.uiState.collectAsState()

    LaunchedEffect(uiState.isAuthenticated) {
        while (uiState.isAuthenticated) {
            delay(5000)
            viewModel.updateRating()
        }
    }

    LaunchedEffect(uiState.isAuthenticated) {
        if (!uiState.isAuthenticated) {
            navController.navigate("login")
        }
    }

    LaunchedEffect(uiState.isSearchGoingOn) {
        if (uiState.isSearchGoingOn) {
            repeat(100) {
                viewModel.getChat()
                delay(2000)
            }
            viewModel.stopSearching()
        }
    }

    LaunchedEffect(uiState.isInterlocutorFound) {
        if (uiState.isInterlocutorFound) {
            navController.navigate("chat")
        }
    }

    Column(
        modifier = Modifier.background(MaterialTheme.colorScheme.background)
    )
    {
        Box(
            modifier = Modifier.fillMaxWidth(),
        ) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth(),
            ) {
                Box(
                    modifier = Modifier
                        .padding(all = 20.dp)
                        .clip(shape = RoundedCornerShape(size = 12.dp))
                        .height(70.dp)
                        .width(230.dp)
                        .background(MaterialTheme.colorScheme.onBackground),
                    contentAlignment = Alignment.CenterStart
                ) {
                    Column {
                        Text(
                            text = uiState.username,
                            style = TextStyle(color = MaterialTheme.colorScheme.background),
                            fontSize = 22.sp,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .padding(horizontal = 15.dp)
                        )
                        Text(text = "Social credits: ${uiState.rating}",
                            style = TextStyle(color = MaterialTheme.colorScheme.background),
                            fontSize = 20.sp,
                            modifier = Modifier
                                .padding(horizontal = 15.dp)
                        )
                    }
                }
                Box(
                    modifier = Modifier
                        .padding(all = 20.dp),
                    contentAlignment = Alignment.TopCenter
                ) {
                    Button(
                        onClick = viewModel::logout,
                        modifier = Modifier
                            .width(70.dp)
                            .height(70.dp),
                        shape = RoundedCornerShape(12.dp)
                    ) {
                        Icon(Icons.Rounded.ExitToApp, contentDescription = "Logout Icon",
                            modifier = Modifier.size(40.dp))
                    }
                }

            }
        }

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(3f),
            contentAlignment = Alignment.Center
        ) {
            Column(
                verticalArrangement = Arrangement.spacedBy(10.dp, alignment = Alignment.Bottom),
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.fillMaxSize()
            ) {
                if(uiState.isSearchGoingOn){
                    CircularProgressIndicator(color = Color.White, modifier = Modifier
                        .size(100.dp)
                        .padding(20.dp)
                    )
                }
                Button (
                    shape = RoundedCornerShape(20.dp),
                    modifier = Modifier
                        .height(160.dp)
                        .fillMaxWidth()
                        .padding(
                            start = 20.dp,
                            end = 20.dp,
                            bottom = 60.dp,
                        ),
                    onClick =  {
                        if (uiState.isSearchGoingOn) viewModel.stopSearching()
                        else viewModel.startSearching()
                    }){
                    if (!uiState.isSearchGoingOn){
                        Text("Find chat", fontSize = 22.sp, color = MaterialTheme.colorScheme.background)
                    } else {
                        Text("Cancel", fontSize = 22.sp)
                    }
                }
            }
        }
    }

}
package com.tuneit.minions.chatapp.data.model.chat

import kotlinx.serialization.Serializable

@Serializable
data class ChatMessagesResponse(
    val messages: List<ChatMessage>
)